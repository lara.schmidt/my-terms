const wordLimit = 250;
const domainLimit = 100;

function tableBody(id) {
  return document.getElementById(id).getElementsByTagName("tbody")[0];
}

function tableBodyRows(id) {
  return tableBody(id).querySelectorAll("tr");
}

function showDomainOptions() {
  const ele = document.getElementById("domain_options");
  ele.style.display = "block";
}

function hideDomainOptions() {
  const ele = document.getElementById("domain_options");
  ele.style.display = "none";
}

function clearLists() {
  tableBody("words").innerHTML = "";
  tableBody("domains").innerHTML = "";
  present();
}

function clearWords() {
  const r = confirm("Are you sure you want to clear all words?");

  if (r == true) {
    getSettings(function (mt) {
      mt.words = [];
      mt.domains = [];
      setSettings(mt);
      hideDomainOptions();
      clearLists();
    });
  }
}

function clearDomains() {
  const r = confirm("Are you sure you want to clear all domains?");

  if (r == true) {
    getSettings(function (mt) {
      mt.domains = [];
      setSettings(mt);
      clearLists();
    });
  }
}

function addWordRow(ndx, findWord, replaceWord, ignoreWordCase) {
  const tbl = tableBody("words");
  const row = tbl.insertRow();

  insertCellWithTextInput(row, ndx, "findWord", findWord.trim());
  insertCellWithTextInput(row, ndx, "replaceWord", replaceWord.trim());
  insertCellWithCheckbox(row, ndx, "ignoreWordCase", ignoreWordCase);
}

function addDomainRow(ndx, domain) {
  const tbl = tableBody("domains");
  const row = tbl.insertRow();
  if (domain && domain.length > 0) {
    try {
      insertCellWithTextInput(row, ndx, "domain", domain);
    } catch (e) {
      alert(`Invalid domain entered: ${domain}. Please retry with valid domain name.`);
    }
  } else {
    insertCellWithTextInput(row, ndx, "domain", domain);
  }
}

function insertCellWithTextInput(row, ndx, prefix, val) {
  const newCell = row.insertCell();
  const inInput = document.createElement("input");
  inInput.type = "text";
  inInput.id = `${prefix}${ndx}`;
  inInput.value = `${val}`;
  inInput.classList.add("word");
  newCell.appendChild(inInput);
}

function insertCellWithCheckbox(row, ndx, prefix, val) {
  const newCell = row.insertCell();

  const inInput = document.createElement("input");
  inInput.type = "checkbox";
  inInput.id = `${prefix}${ndx}`;
  inInput.checked = val;

  const text = document.createElement("span");
  text.appendChild(document.createTextNode("Ignore Case"));

  const label = document.createElement("label");
  label.classList.add("checkbox");
  label.htmlFor = `${prefix}${ndx}`;

  label.appendChild(inInput);
  label.appendChild(text);
  newCell.appendChild(label);
}

function newSettings() {
  return { words: [], domains: [] };
}

function saveSettings() {
  const settings = newSettings();

  const words = tableBodyRows("words");
  for (let i = 0; i < words.length; i++) {
    const findWord = document.getElementById(`findWord${i}`).value;
    const replaceWord = document.getElementById(`replaceWord${i}`).value;
    const ignoreCase = document.getElementById(`ignoreWordCase${i}`).checked;
    settings.words.push({ find: findWord, replace: replaceWord, ignoreCase: ignoreCase });
  }

  const domains = tableBodyRows("domains");
  for (let i = 0; i < domains.length; i++) {
    const val = document.getElementById(`domain${i}`).value;
    if (val && val.length > 0) {
      try {
        const url = new URL(val);
        settings.domains.push(url.toString());
      } catch (e) {
        alert("Error saving domain, please ensure proper format of https://mydomain.com", e);
        return false;
      }
    }
  }

  setSettings(settings);
  return true;
}

function getSettings(callback) {
  chrome.storage.sync.get("myTerms", function (settings) {
    if (settings && settings.myTerms) {
      callback(settings.myTerms);
    } else {
      callback(newSettings());
    }
  });
}

function addWord() {
  const rows = tableBodyRows("words");

  if (rows && rows.length <= wordLimit) {
    if (saveSettings()) {
      showDomainOptions();

      addWordRow(rows.length, "", "", false);
    }
  } else {
    alert(`Reached limit of ${wordLimit} words.`);
  }
}

function addDomain() {
  const rows = tableBodyRows("domains");

  if (rows && rows.length <= domainLimit) {
    if (saveSettings()) {
      addDomainRow(rows.length, "");
    }
  } else {
    alert(`Reached limit of ${domainLimit} domains.`);
  }
}

function present() {
  getSettings(function (mt) {
    if (mt.words && mt.words.length > 0) {
      showDomainOptions();
      for (let i = 0; i < mt.words.length; i++) {
        let word = mt.words[i];
        addWordRow(i, word.find, word.replace, word.ignoreCase);
      }
    } else {
      addWordRow(0, "", "", false);
    }

    if (mt.domains && mt.domains.length > 0) {
      for (let i = 0; i < mt.domains.length; i++) {
        addDomainRow(i, mt.domains[i]);
      }
    } else {
      addDomainRow(0, "");
    }
  });
}

function setSettings(mt) {
  chrome.storage.sync.set({ myTerms: mt }, function () {
    // Update status to let user know options were saved.
    var status = document.getElementById("status");
    status.textContent = "Options saved.";
    setTimeout(function () {
      status.textContent = "";
    }, 3000);
  });
}

document.addEventListener("DOMContentLoaded", () => {
  document.getElementById("addWord").addEventListener("click", addWord);
  document.getElementById("clearWords").addEventListener("click", clearWords);
  document.getElementById("addDomain").addEventListener("click", addDomain);
  document.getElementById("clearDomains").addEventListener("click", clearDomains);

  const saves = document.getElementsByClassName("save");
  for (var i = 0; i < saves.length; i++) {
    saves[i].addEventListener("click", saveSettings);
  }
  present();
});
